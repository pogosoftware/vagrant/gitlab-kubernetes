# Deploy apps to Kubernetes cluster using Gitlab

# Docs

* [Multi project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html)
* [Pipeline atchitecture](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html)
* [Environments](https://docs.gitlab.com/ee/ci/environments/)
