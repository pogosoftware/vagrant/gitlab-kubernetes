# Gitlab setup

## Develop

| Filed name | Value|
|------------|------|
| Kubernetes cluster name | k3s-develop |
| Environment scope | develop |
| API URL | `kubectl cluster-info | grep 'Kubernetes control plane' | awk '/https/ {print $NF}'` |
| CA Certificate | `kubectl get secret $(kubectl get secrets -o jsonpath="{['items'][0]['metadata']['name']}") -o jsonpath="{['data']['ca\.crt']}" | base64 --decode` |
| Service Token: | `kubectl -n release-management get secret $(kubectl -n release-management get secret | grep gitlab-token | awk '{print $1}') -o jsonpath="{['data']['token']}" | base64 --decode` |
| GitLab-managed cluster | false |
| Namespace per environment | false |

<br>

## Production

| Filed name | Value|
|------------|------|
| Kubernetes cluster name | k3s-production |
| Environment scope | production |
| API URL | `kubectl cluster-info | grep 'Kubernetes control plane' | awk '/https/ {print $NF}'` |
| CA Certificate | `kubectl get secret $(kubectl get secrets -o jsonpath="{['items'][0]['metadata']['name']}") -o jsonpath="{['data']['ca\.crt']}" | base64 --decode` |
| Service Token: | `kubectl -n release-management get secret $(kubectl -n release-management get secret | grep gitlab-token | awk '{print $1}') -o jsonpath="{['data']['token']}" | base64 --decode` |
| GitLab-managed cluster | false |
| Namespace per environment | false |
