# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  
  ####################################################################################################
  ### K3S Server Node
  ####################################################################################################
  config.vm.define "k3s-server" do |instance|
    instance.vm.box = "bento/ubuntu-20.04"
    instance.vm.hostname = "server"
    instance.vm.network "private_network", ip: "10.128.10.20"

    instance.vm.provider "virtualbox" do |vb|
      vb.gui    = false
      vb.name   = "K3S Server"
      vb.cpus   = 1
      vb.memory = 1024
    end

    instance.vm.provision "shell", inline: <<-SHELL
      apt update
      
      # Install K3S Server Node (https://rancher.com/docs/k3s/latest/en/installation/install-options/server-config/)
      curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_CHANNEL=latest sh -s - server --node-ip 10.128.10.20 --bind-address 10.128.10.20 --https-listen-port 8443 --flannel-backend=none --cluster-cidr=192.168.0.0/16 --disable-network-policy --disable=traefik

      # Deploy Callico netowork
      kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml
      kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml

      # Copy to shared volume K3S node token
      cp /var/lib/rancher/k3s/server/node-token /vagrant/token
    SHELL

    instance.vm.provision "shell", privileged: false, inline: <<-SHELL
      # Create namespaces
      kubectl create ns release-management
      kubectl create ns dev
      kubectl create ns prod

      # Create service accounts and permissions
      #kubectl create -f /vagrant/files/gitlab-runner-rbac.yaml
      #kubectl create -f /vagrant/files/gitlab-runner-kubernetes-rbac.yaml

      # Install Helm
      curl -sfL https://get.helm.sh/helm-v3.5.2-linux-amd64.tar.gz | tar xvz
      sudo mv linux-amd64/helm /usr/local/bin/helm
      sudo chmod +x /usr/local/bin/helm
      rm -rf linux-amd64
      
      # Install helm diff plugin
      helm plugin install https://github.com/databus23/helm-diff

      # Install helmfile
      wget --quiet https://github.com/roboll/helmfile/releases/download/v0.138.4/helmfile_linux_amd64
      sudo mv helmfile_linux_amd64 /usr/local/bin/helmfile
      sudo chmod +x /usr/local/bin/helmfile
    SHELL
  end

  ####################################################################################################
  ### K3S Agent Node
  ####################################################################################################
  config.vm.define "k3s-agent" do |instance|
    instance.vm.box = "bento/ubuntu-20.04"
    instance.vm.hostname = "agent"
    instance.vm.network "private_network", ip: "10.128.10.30"

    instance.vm.provider "virtualbox" do |vb|
      vb.gui    = false
      vb.name   = "K3S Agent"
      vb.cpus   = 1
      vb.memory = 1024
    end

    instance.vm.provision "shell", inline: <<-SHELL
      apt update

      # Install K3S Agent Node (https://rancher.com/docs/k3s/latest/en/installation/install-options/agent-config/)
      curl -sfL https://get.k3s.io | INSTALL_K3S_CHANNEL=latest sh -s - agent --token-file /vagrant/token --server https://10.128.10.20:8443
    SHELL
  end

  ####################################################################################################
  ### Gitlab Server
  ####################################################################################################
  config.vm.define "gitlab" do |instance|
    instance.vm.box = "pogosoftware/docker"
    instance.vm.hostname = "gitlab"
    instance.vm.network "private_network", ip: "10.128.10.10"

    instance.vm.provider "virtualbox" do |vb|
      vb.gui    = false
      vb.name   = "Gitlab Server"
      vb.cpus   = 2
      vb.memory = 4096
    end

    instance.vm.provision "shell", inline: <<-SHELL
      apt update
      docker-compose -f /vagrant/files/gitlab-ce.yml up -d
    SHELL
  end
end
